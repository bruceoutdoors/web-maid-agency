﻿Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Return View()
    End Function

    Function About() As ActionResult
        ViewData("Message") = "Maid Agency System description page."

        Return View()
    End Function

    Function Contact() As ActionResult
        ViewData("Message") = "Maid Agency System contact page."

        Return View()
    End Function
End Class
