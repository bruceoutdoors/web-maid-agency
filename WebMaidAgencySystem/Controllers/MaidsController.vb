﻿Imports System
Imports System.Collections.Generic
Imports System.Data
Imports System.Data.Entity
Imports System.Linq
Imports System.Net
Imports System.Web
Imports System.Web.Mvc
Imports WebMaidAgencySystem

Namespace Controllers
    Public Class MaidsController
        Inherits System.Web.Mvc.Controller

        Private db As New MyDbContext

        ' GET: Maids
        Function Index() As ActionResult
            Dim maids = db.Maids.Include(Function(m) m.User)
            Return View(maids.ToList())
        End Function

        ' GET: Maids/Details/5
        Function Details(ByVal id As Integer?) As ActionResult
            If IsNothing(id) Then
                Return New HttpStatusCodeResult(HttpStatusCode.BadRequest)
            End If
            Dim maid As Maid = db.Maids.Find(id)
            If IsNothing(maid) Then
                Return HttpNotFound()
            End If
            Return View(maid)
        End Function

        ' GET: Maids/Create
        Function Create() As ActionResult
            ViewBag.UserID = New SelectList(db.Users, "Id", "user_name")
            Return View()
        End Function

        ' POST: Maids/Create
        'To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        'more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        <HttpPost()>
        <ValidateAntiForgeryToken()>
        Function Create(<Bind(Include:="Id,maid_name,birth_date,country,cost_monthly,UserID,employment_date")> ByVal maid As Maid) As ActionResult
            If ModelState.IsValid Then
                db.Maids.Add(maid)
                db.SaveChanges()
                Return RedirectToAction("Index")
            End If
            ViewBag.UserID = New SelectList(db.Users, "Id", "user_name", maid.UserID)
            Return View(maid)
        End Function

        ' GET: Maids/Edit/5
        Function Edit(ByVal id As Integer?) As ActionResult
            If IsNothing(id) Then
                Return New HttpStatusCodeResult(HttpStatusCode.BadRequest)
            End If
            Dim maid As Maid = db.Maids.Find(id)
            If IsNothing(maid) Then
                Return HttpNotFound()
            End If

            Dim selectlist = New SelectList(db.Users, "Id", "user_name", maid.UserID)
            ViewBag.UserID = selectlist
            Return View(maid)
        End Function

        ' POST: Maids/Edit/5
        'To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        'more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        <HttpPost()>
        <ValidateAntiForgeryToken()>
        Function Edit(<Bind(Include:="Id,maid_name,birth_date,country,cost_monthly,UserID,employment_date")> ByVal maid As Maid) As ActionResult
            If ModelState.IsValid Then
                db.Entry(maid).State = EntityState.Modified
                db.SaveChanges()
                Return RedirectToAction("Index")
            End If
            ViewBag.UserID = New SelectList(db.Users, "Id", "user_name", maid.UserID)
            Return View(maid)
        End Function

        ' GET: Maids/Delete/5
        Function Delete(ByVal id As Integer?) As ActionResult
            If IsNothing(id) Then
                Return New HttpStatusCodeResult(HttpStatusCode.BadRequest)
            End If
            Dim maid As Maid = db.Maids.Find(id)
            If IsNothing(maid) Then
                Return HttpNotFound()
            End If
            Return View(maid)
        End Function

        ' POST: Maids/Delete/5
        <HttpPost()>
        <ActionName("Delete")>
        <ValidateAntiForgeryToken()>
        Function DeleteConfirmed(ByVal id As Integer) As ActionResult
            Dim maid As Maid = db.Maids.Find(id)
            db.Maids.Remove(maid)
            db.SaveChanges()
            Return RedirectToAction("Index")
        End Function

        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If (disposing) Then
                db.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        Function Employ(ByVal id As Integer) As ActionResult
            Dim maid As Maid = db.Maids.Find(id)
            If IsNothing(maid) Then
                Return HttpNotFound()
            End If

            If System.Web.HttpContext.Current.Session("username") Is Nothing Then
                Return RedirectToAction("Login", "Account",
                                        New With {.ReturnUrl = "/Maids/Details/" & id})
            End If

            maid.UserID = System.Web.HttpContext.Current.Session("userid")
            maid.employment_date = Date.Now
            db.SaveChanges()

            Dim inv As New Invoice
            inv.invoice_date = Date.Now
            inv.total_amount = maid.cost_monthly
            inv.UserID = System.Web.HttpContext.Current.Session("userid")
            inv.MaidID = maid.Id

            Dim addedinv = db.Invoices.Add(inv)
            db.SaveChanges()

            Return RedirectToAction("Details", "Invoices", New With {.id = addedinv.Id})
        End Function
    End Class
End Namespace
