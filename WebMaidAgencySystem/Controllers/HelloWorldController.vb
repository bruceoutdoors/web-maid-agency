﻿Imports System.Web.Mvc

Namespace Controllers
    Public Class HelloWorldController
        Inherits Controller

        Function Index() As ActionResult
            Return View()
        End Function

        Function Welcome(name As String, Optional numTimes As Integer = 1) As String
            Return HttpUtility.HtmlEncode("Hello " & name & ", NumTimes is: " & numTimes)
        End Function

    End Class
End Namespace