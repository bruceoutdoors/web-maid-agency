﻿Imports System
Imports System.Collections.Generic
Imports System.Data.Entity
Imports System.ComponentModel.DataAnnotations

Public Class User

    Public Sub User()
        subcribeNewsletter = True
    End Sub


    Public Property Id As Integer
    <Display(Name:="Username")>
    <Required>
    Public Property user_name As String
    <Required>
    <Display(Name:="Password")>
    Public Property password As String
    <Required>
    <EmailAddress>
    <Display(Name:="Email")>
    Public Property email As String
    <Required>
    <Display(Name:="Address 1")>
    Public Property address1 As String
    <Display(Name:="Address 2")>
    Public Property address2 As String
    <Display(Name:="City")>
    Public Property city As String
    <Display(Name:="Postcode")>
    Public Property postcode As String
    <Required>
    <Display(Name:="Telephone")>
    Public Property telephone As String
    <Display(Name:="Subcribe To Our Newsletter")>
    Public Property subcribeNewsletter As Boolean
    Public Property userTypeID As Integer
    Public Overridable Property Invoices As ICollection(Of Invoice) = New HashSet(Of Invoice)
    Public Overridable Property Maids As ICollection(Of Maid) = New HashSet(Of Maid)
    Public Overridable Property UserType As UserType

End Class

