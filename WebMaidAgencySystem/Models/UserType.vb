﻿Imports System
Imports System.Collections.Generic
Imports System.Data.Entity

Public Class UserType
    Public Property Id As Integer
    Public Property type As String

    Public Overridable Property Users As ICollection(Of User) = New HashSet(Of User)

End Class
