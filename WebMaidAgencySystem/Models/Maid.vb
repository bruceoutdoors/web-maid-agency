﻿Imports System
Imports System.Collections.Generic
Imports System.Data.Entity
Imports System.ComponentModel.DataAnnotations


Public Class Maid
    Public Property Id As Integer
    <Display(Name:="Maid Name")>
    Public Property maid_name As String

    <DisplayFormat(DataFormatString:="{0:dd/MM/yyyy}", _
               ApplyFormatInEditMode:=True)>
    <Display(Name:="Birth Date")>
    Public Property birth_date As Nullable(Of Date)
    Public Property country As String
    <Display(Name:="Monthly Cost")>
    Public Property cost_monthly As Decimal
    Public Property UserID As Nullable(Of Integer)
    <Display(Name:="Employment Date")>
    Public Property employment_date As Nullable(Of Date)


    Public Overridable Property User As User


End Class


