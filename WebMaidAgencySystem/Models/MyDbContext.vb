﻿Imports System.Data.Entity

Public Class MyDbContext
    Inherits DbContext

    Public Property UserTypes As DbSet(Of UserType)
    Public Property Users As DbSet(Of User)
    Public Property Maids As DbSet(Of Maid)
    Public Property Invoices As DbSet(Of Invoice)

End Class
