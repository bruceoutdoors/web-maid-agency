﻿Public NotInheritable Class Account
    Private Sub New()
    End Sub

    Public Shared Function IsAdmin() As Boolean
        If System.Web.HttpContext.Current.Session("username") IsNot Nothing And _
                System.Web.HttpContext.Current.Session("usertype") = 1 Then
            Return True
        End If

        Return False
    End Function

    Public Shared Function IsUser() As Boolean
        If System.Web.HttpContext.Current.Session("username") IsNot Nothing And _
                System.Web.HttpContext.Current.Session("usertype") = 2 Then
            Return True
        End If

        Return False
    End Function

    Public Shared Function IsLoggedIn() As Boolean
        If System.Web.HttpContext.Current.Session("username") IsNot Nothing Then
            Return True
        End If

        Return False
    End Function
End Class