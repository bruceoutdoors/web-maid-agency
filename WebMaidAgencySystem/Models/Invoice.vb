﻿Imports System
Imports System.Collections.Generic
Imports System.Data.Entity
Imports System.ComponentModel.DataAnnotations

Public Class Invoice
    Public Property Id As Integer
    <Display(Name:="Total Amount Due")>
    Public Property total_amount As Nullable(Of Decimal)
    <Display(Name:="Invoice Date")>
    Public Property invoice_date As Date
    <Display(Name:="Paid Date")>
    Public Property paid_date As Nullable(Of Date)
    Public Property UserID As Integer
    Public Property MaidID As Integer


    Public Overridable Property User As User
    Public Overridable Property Maid As Maid

End Class

