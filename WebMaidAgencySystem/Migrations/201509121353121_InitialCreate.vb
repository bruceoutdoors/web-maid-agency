Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class InitialCreate
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.Invoice_Maid",
                Function(c) New With
                    {
                        .Id = c.Int(nullable := False, identity := True),
                        .InvoiceID = c.Int(nullable := False),
                        .MaidID = c.Int(nullable := False),
                        .amount = c.Decimal(precision := 18, scale := 2)
                    }) _
                .PrimaryKey(Function(t) t.Id) _
                .ForeignKey("dbo.Invoices", Function(t) t.InvoiceID, cascadeDelete := True) _
                .ForeignKey("dbo.Maids", Function(t) t.MaidID, cascadeDelete := True) _
                .Index(Function(t) t.InvoiceID) _
                .Index(Function(t) t.MaidID)
            
            CreateTable(
                "dbo.Invoices",
                Function(c) New With
                    {
                        .Id = c.Int(nullable := False, identity := True),
                        .total_amount = c.Decimal(precision := 18, scale := 2),
                        .invoice_date = c.DateTime(nullable := False),
                        .paid_date = c.DateTime(),
                        .UserID = c.Int(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.Id) _
                .ForeignKey("dbo.Users", Function(t) t.UserID, cascadeDelete := True) _
                .Index(Function(t) t.UserID)
            
            CreateTable(
                "dbo.Users",
                Function(c) New With
                    {
                        .Id = c.Int(nullable := False, identity := True),
                        .user_name = c.String(),
                        .password = c.String(),
                        .userTypeID = c.Int(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.Id) _
                .ForeignKey("dbo.UserTypes", Function(t) t.userTypeID, cascadeDelete := True) _
                .Index(Function(t) t.userTypeID)
            
            CreateTable(
                "dbo.Maids",
                Function(c) New With
                    {
                        .Id = c.Int(nullable := False, identity := True),
                        .maid_name = c.String(),
                        .birth_date = c.DateTime(),
                        .country = c.String(),
                        .cost_monthly = c.Decimal(nullable := False, precision := 18, scale := 2),
                        .UserID = c.Int(),
                        .employment_date = c.DateTime()
                    }) _
                .PrimaryKey(Function(t) t.Id) _
                .ForeignKey("dbo.Users", Function(t) t.UserID) _
                .Index(Function(t) t.UserID)
            
            CreateTable(
                "dbo.UserTypes",
                Function(c) New With
                    {
                        .Id = c.Int(nullable := False, identity := True),
                        .type = c.String()
                    }) _
                .PrimaryKey(Function(t) t.Id)
            
        End Sub
        
        Public Overrides Sub Down()
            DropForeignKey("dbo.Users", "userTypeID", "dbo.UserTypes")
            DropForeignKey("dbo.Maids", "UserID", "dbo.Users")
            DropForeignKey("dbo.Invoice_Maid", "MaidID", "dbo.Maids")
            DropForeignKey("dbo.Invoices", "UserID", "dbo.Users")
            DropForeignKey("dbo.Invoice_Maid", "InvoiceID", "dbo.Invoices")
            DropIndex("dbo.Maids", New String() { "UserID" })
            DropIndex("dbo.Users", New String() { "userTypeID" })
            DropIndex("dbo.Invoices", New String() { "UserID" })
            DropIndex("dbo.Invoice_Maid", New String() { "MaidID" })
            DropIndex("dbo.Invoice_Maid", New String() { "InvoiceID" })
            DropTable("dbo.UserTypes")
            DropTable("dbo.Maids")
            DropTable("dbo.Users")
            DropTable("dbo.Invoices")
            DropTable("dbo.Invoice_Maid")
        End Sub
    End Class
End Namespace
