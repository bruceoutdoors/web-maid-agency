Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class InvoiceAddMaidID
        Inherits DbMigration
    
        Public Overrides Sub Up()
            DropForeignKey("dbo.Invoice_Maid", "InvoiceID", "dbo.Invoices")
            DropForeignKey("dbo.Invoice_Maid", "MaidID", "dbo.Maids")
            DropIndex("dbo.Invoice_Maid", New String() { "InvoiceID" })
            DropIndex("dbo.Invoice_Maid", New String() { "MaidID" })
            AddColumn("dbo.Invoices", "MaidID", Function(c) c.Int(nullable := False))
            CreateIndex("dbo.Invoices", "MaidID")
            AddForeignKey("dbo.Invoices", "MaidID", "dbo.Maids", "Id", cascadeDelete := True)
            DropTable("dbo.Invoice_Maid")
        End Sub
        
        Public Overrides Sub Down()
            CreateTable(
                "dbo.Invoice_Maid",
                Function(c) New With
                    {
                        .Id = c.Int(nullable := False, identity := True),
                        .InvoiceID = c.Int(nullable := False),
                        .MaidID = c.Int(nullable := False),
                        .amount = c.Decimal(precision := 18, scale := 2)
                    }) _
                .PrimaryKey(Function(t) t.Id)
            
            DropForeignKey("dbo.Invoices", "MaidID", "dbo.Maids")
            DropIndex("dbo.Invoices", New String() { "MaidID" })
            DropColumn("dbo.Invoices", "MaidID")
            CreateIndex("dbo.Invoice_Maid", "MaidID")
            CreateIndex("dbo.Invoice_Maid", "InvoiceID")
            AddForeignKey("dbo.Invoice_Maid", "MaidID", "dbo.Maids", "Id", cascadeDelete := True)
            AddForeignKey("dbo.Invoice_Maid", "InvoiceID", "dbo.Invoices", "Id", cascadeDelete := True)
        End Sub
    End Class
End Namespace
