Imports System
Imports System.Data.Entity
Imports System.Data.Entity.Migrations
Imports System.Linq

Namespace Migrations

    Friend NotInheritable Class Configuration 
        Inherits DbMigrationsConfiguration(Of MyDbContext)

        Public Sub New()
            AutomaticMigrationsEnabled = True
            ContextKey = "WebMaidAgencySystem.MyDbContext"
        End Sub

        Protected Overrides Sub Seed(context As MyDbContext)
            '  This method will be called after migrating to the latest version.

            '  You can use the DbSet(Of T).AddOrUpdate() helper extension method 
            '  to avoid creating duplicate seed data. E.g.
            '
            context.UserTypes.AddOrUpdate(Function(x) x.Id,
               New UserType() With {.Id = 1, .type = "admin"},
               New UserType() With {.Id = 2, .type = "user"}
               )


            context.Users.AddOrUpdate(Function(x) x.Id,
               New User() With {
                   .Id = 1,
                   .user_name = "admin",
                   .password = "admin",
                   .userTypeID = 1,
                   .telephone = "0163188854",
                   .postcode = "47610",
                   .email = "shaungoh01@gmail.com",
                   .address1 = "21, USJ 5/1F, Selangor or something like that",
                   .city = "Pompei"
               },
               New User() With {
                   .Id = 2,
                   .user_name = "bruce",
                   .password = "bruce",
                   .userTypeID = 2,
                   .telephone = "0163188854",
                   .postcode = "47610",
                   .email = "shaungoh01@gmail.com",
                   .address1 = "21, USJ 5/1F, Selangor or something like that",
                   .city = "Pompei"
               },
               New User() With {
                   .Id = 3,
                   .user_name = "hii",
                   .password = "hii",
                   .userTypeID = 2,
                   .telephone = "0163188854",
                   .postcode = "47610",
                   .email = "shaungoh01@gmail.com",
                   .address1 = "21, USJ 5/1F, Selangor or something like that",
                   .city = "Pompei"
               },
               New User() With {
                   .Id = 4,
                   .user_name = "shaun",
                   .password = "shaun",
                   .userTypeID = 2,
                   .telephone = "0163188854",
                   .postcode = "47610",
                   .email = "shaungoh01@gmail.com",
                   .address1 = "21, USJ 5/1F, Selangor or something like that",
                   .city = "Pompei"
               }
               )

            context.Maids.AddOrUpdate(Function(x) x.Id,
                New Maid() With {
                    .Id = 1,
                    .maid_name = "Jessica Alba",
                    .birth_date = New Date(1985, 11, 13),
                    .country = "America",
                    .cost_monthly = 5000.0,
                    .UserID = 3,
                    .employment_date = New Date(2015, 4, 4)
                },
                New Maid() With {
                    .Id = 2,
                    .maid_name = "Marnie Edgar",
                    .birth_date = New Date(1992, 11, 13),
                    .country = "Indonesia",
                    .cost_monthly = 1231.0,
                    .UserID = 4,
                    .employment_date = New Date(2015, 5, 5)
                },
                New Maid() With {
                    .Id = 3,
                    .maid_name = "Samantha Baker",
                    .birth_date = New Date(1994, 11, 13),
                    .country = "Indonesia",
                    .cost_monthly = 1544.0
                },
                New Maid() With {
                    .Id = 4,
                    .maid_name = "Vivian Ward",
                    .birth_date = New Date(1989, 11, 13),
                    .country = "America",
                    .cost_monthly = 4000.0
                },
                New Maid() With {
                    .Id = 5,
                    .maid_name = "Cherry Darling",
                    .birth_date = New Date(1994, 8, 21),
                    .country = "America",
                    .cost_monthly = 3250.0
                },
                New Maid() With {
                    .Id = 6,
                    .maid_name = "Marquise de Merteuil",
                    .birth_date = New Date(1988, 5, 6),
                    .country = "France",
                    .cost_monthly = 4520.0
                },
                New Maid() With {
                    .Id = 7,
                    .maid_name = "Karen Silkwood",
                    .birth_date = New Date(1987, 2, 15),
                    .country = "Malaysia",
                    .cost_monthly = 2410.0
                }
                )

            context.Invoices.AddOrUpdate(Function(x) x.Id,
                New Invoice() With {
                    .Id = 1,
                    .invoice_date = Date.Now,
                    .MaidID = 1,
                    .total_amount = 5000,
                    .UserID = 3
                },
            New Invoice() With {
                    .Id = 2,
                    .invoice_date = Date.Now,
                    .MaidID = 2,
                    .total_amount = 1231,
                    .UserID = 4
                }
                )
        End Sub

    End Class

End Namespace
