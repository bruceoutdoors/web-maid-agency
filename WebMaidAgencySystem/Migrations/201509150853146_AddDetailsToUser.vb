Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class AddDetailsToUser
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Users", "email", Function(c) c.String(nullable := False))
            AddColumn("dbo.Users", "address1", Function(c) c.String(nullable := False))
            AddColumn("dbo.Users", "address2", Function(c) c.String())
            AddColumn("dbo.Users", "city", Function(c) c.String())
            AddColumn("dbo.Users", "postcode", Function(c) c.String())
            AddColumn("dbo.Users", "telephone", Function(c) c.String(nullable := False))
            AddColumn("dbo.Users", "subcribeNewsletter", Function(c) c.Boolean(nullable := False))
            AlterColumn("dbo.Users", "user_name", Function(c) c.String(nullable := False))
            AlterColumn("dbo.Users", "password", Function(c) c.String(nullable := False))
        End Sub
        
        Public Overrides Sub Down()
            AlterColumn("dbo.Users", "password", Function(c) c.String())
            AlterColumn("dbo.Users", "user_name", Function(c) c.String())
            DropColumn("dbo.Users", "subcribeNewsletter")
            DropColumn("dbo.Users", "telephone")
            DropColumn("dbo.Users", "postcode")
            DropColumn("dbo.Users", "city")
            DropColumn("dbo.Users", "address2")
            DropColumn("dbo.Users", "address1")
            DropColumn("dbo.Users", "email")
        End Sub
    End Class
End Namespace
