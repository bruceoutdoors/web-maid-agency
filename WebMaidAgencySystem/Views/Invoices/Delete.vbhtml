﻿@ModelType WebMaidAgencySystem.Invoice
@Code
    ViewData("Title") = "Delete"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

<h2>Delete</h2>

<h3>Are you sure you want to delete this?</h3>
<div>
    <h4>Invoice</h4>
    <hr />
    <dl class="dl-horizontal">
        <dt>
            @Html.DisplayNameFor(Function(model) model.Maid.maid_name)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Maid.maid_name)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.User.user_name)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.User.user_name)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.total_amount)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.total_amount)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.invoice_date)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.invoice_date)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.paid_date)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.paid_date)
        </dd>

    </dl>
    @Using (Html.BeginForm())
        @Html.AntiForgeryToken()

        @<div class="form-actions no-color">
            <input type="submit" value="Delete" class="btn btn-default" /> |
            @Html.ActionLink("Back to List", "Index")
        </div>
    End Using
</div>
