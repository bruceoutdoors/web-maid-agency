﻿@ModelType WebMaidAgencySystem.Invoice
@Code
    ViewData("Title") = "Details"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

<h2>Details</h2>

<div>
    <h4>Invoice</h4>
    <hr />
    <dl class="dl-horizontal">
        <dt>
            @Html.DisplayNameFor(Function(model) model.Maid.maid_name)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Maid.maid_name)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.User.user_name)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.User.user_name)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.total_amount)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.total_amount)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.invoice_date)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.invoice_date)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.paid_date)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.paid_date)
        </dd>

    </dl>
</div>
<p>
    @If Account.IsAdmin Then
        @Html.ActionLink("Edit", "Edit", New With {.id = Model.Id}) @(" | ")
    End If
    
    @Html.ActionLink("Back to List", "Index")
</p>
