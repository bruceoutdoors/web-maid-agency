﻿@ModelType IEnumerable(Of WebMaidAgencySystem.Invoice)
@Code
ViewData("Title") = "Index"
Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

<h2>Index</h2>
@If Account.IsAdmin Then
    @<p>
    @Html.ActionLink("Create New", "Create")
    </p>
End If
<table class="table">
    <tr>
        <th>
            @Html.DisplayNameFor(Function(model) model.Maid.maid_name)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.User.user_name)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.total_amount)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.invoice_date)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.paid_date)
        </th>
        <th></th>
    </tr>
@Code
    Dim invList As IEnumerable(Of WebMaidAgencySystem.Invoice)
    If Account.IsAdmin Then
        invList = Model
    ElseIf System.Web.HttpContext.Current.Session("userid") IsNot Nothing Then
        invList = Model.Where(Function(q) q.UserID = System.Web.HttpContext.Current.Session("userid"))
    Else
        invList = Model
    End If
End Code
@For Each item In invList
    @<tr>
        <td>
            @Html.DisplayFor(Function(modelItem) item.Maid.maid_name)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.User.user_name)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.total_amount)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.invoice_date)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.paid_date)
        </td>
        <td>
            @Html.ActionLink("Details", "Details", New With {.id = item.Id })
            
            @If Account.IsAdmin Then
                @(" | ")
                @Html.ActionLink("Edit", "Edit", New With {.id = item.Id}) @(" | ")
                @Html.ActionLink("Delete", "Delete", New With {.id = item.Id})
            End If
        </td>
    </tr>
Next

</table>
