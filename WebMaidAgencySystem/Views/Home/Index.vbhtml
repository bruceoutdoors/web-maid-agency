﻿@Code
    ViewData("Title") = "Home Page"
    Dim maidTxt As String
    Dim invoiceTxt As String
    
    maidTxt = "Search for Maids"
    invoiceTxt = "See invoices"
    Dim welcomeWho As String
    welcomeWho = "stranger"
    
    If Account.IsLoggedIn Then
        welcomeWho = System.Web.HttpContext.Current.Session("username")
    End If
    
    If Account.IsAdmin Then
        maidTxt = "Manage Maids"
        invoiceTxt = "Manage Invoices"
    ElseIf Account.IsUser Then
        invoiceTxt = "See invoices"
    End If
End Code

<div class="jumbotron">
    <h1>Maid Agency System</h1>
    <p class="lead">The best maid agency system on planet earth! :D</p>
    <p>Hello @welcomeWho, what would you like to do today?</p>
    @If Account.IsLoggedIn Then
        @<ul>
            <li>@Html.ActionLink(maidTxt, "Index", "Maids")</li>
            @If Account.IsLoggedIn Then
                @<li>@Html.ActionLink(invoiceTxt, "Index", "Invoices")</li>
            End If

            @If Account.IsAdmin Then
                @<li>@Html.ActionLink("Manage Users", "Index", "Users")</li>
            End If
        </ul>
    Else
        @<p>Please Register/login to do anything in this system</p>
    End If
    
    
   
</div>


