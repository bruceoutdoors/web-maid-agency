﻿@Code
    ViewData("Title") = "About"
End Code

<h2>@ViewData("Title").</h2>
<h3>@ViewData("Message")</h3>

<p>ViTrox Technologies is a Penang, Malaysia based company that specializes in designing and developing automated vision inspection system and equipment testers for the semiconductor and electronic packaging industries as well as electronic communications equipment. The name Vitrox reflects the core business of the company which is machine vision and electronics.</p>

<p>With the purchase of AXI and AOI from Agilent, ViTrox Technologies now have 4 different segments of business units which are system integrator and solutions provider of high-speed machine vision inspection systems, automated optical inspection, X-Ray inspection and electronics communication systems to cater different kind of vision test for different segments of customer bases in semiconductor related industries worldwide. Vitrox key customers are in the back-end semi-conductor assembly and packaging industry as well as the electronics manufacturing and contract manufacturing industry</p>
