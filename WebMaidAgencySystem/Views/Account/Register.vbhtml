﻿@ModelType RegisterViewModel
@Code
    ViewBag.Title = "Register"
End Code

<h2>@ViewBag.Title.</h2>

@Using Html.BeginForm("Register", "Account", FormMethod.Post, New With {.class = "form-horizontal", .role = "form"})

    @Html.AntiForgeryToken()

    @<text>
    <h4>Create a new account.</h4>
    <hr />
    @Html.ValidationSummary("", New With {.class = "text-danger"})
    <div class="form-group">
        @Html.LabelFor(Function(m) m.Email, New With {.class = "col-md-2 control-label"})
        <div class="col-md-10">
            @Html.TextBoxFor(Function(m) m.Email, New With {.class = "form-control"})
        </div>
    </div>
<div class="form-group">
    @Html.LabelFor(Function(model) model.realemail, htmlAttributes:=New With {.class = "control-label col-md-2"})
    <div class="col-md-10">
        @Html.EditorFor(Function(model) model.realemail, New With {.htmlAttributes = New With {.class = "form-control"}})
    </div>
</div>
    <div class="form-group">
        @Html.LabelFor(Function(m) m.Password, New With {.class = "col-md-2 control-label"})
        <div class="col-md-10">
            @Html.PasswordFor(Function(m) m.Password, New With {.class = "form-control"})
        </div>
    </div>
    <div class="form-group">
        @Html.LabelFor(Function(m) m.ConfirmPassword, New With {.class = "col-md-2 control-label"})
        <div class="col-md-10">
            @Html.PasswordFor(Function(m) m.ConfirmPassword, New With {.class = "form-control"})
        </div>
    </div>

<div class="form-group">
    @Html.LabelFor(Function(model) model.address1, htmlAttributes:=New With {.class = "control-label col-md-2"})
    <div class="col-md-10">
        @Html.EditorFor(Function(model) model.address1, New With {.htmlAttributes = New With {.class = "form-control"}})
        @Html.ValidationMessageFor(Function(model) model.address1, "", New With {.class = "text-danger"})
    </div>
</div>

<div class="form-group">
    @Html.LabelFor(Function(model) model.address2, htmlAttributes:=New With {.class = "control-label col-md-2"})
    <div class="col-md-10">
        @Html.EditorFor(Function(model) model.address2, New With {.htmlAttributes = New With {.class = "form-control"}})
        @Html.ValidationMessageFor(Function(model) model.address2, "", New With {.class = "text-danger"})
    </div>
</div>

<div class="form-group">
    @Html.LabelFor(Function(model) model.city, htmlAttributes:=New With {.class = "control-label col-md-2"})
    <div class="col-md-10">
        @Html.EditorFor(Function(model) model.city, New With {.htmlAttributes = New With {.class = "form-control"}})
        @Html.ValidationMessageFor(Function(model) model.city, "", New With {.class = "text-danger"})
    </div>
</div>

<div class="form-group">
    @Html.LabelFor(Function(model) model.postcode, htmlAttributes:=New With {.class = "control-label col-md-2"})
    <div class="col-md-10">
        @Html.EditorFor(Function(model) model.postcode, New With {.htmlAttributes = New With {.class = "form-control"}})
        @Html.ValidationMessageFor(Function(model) model.postcode, "", New With {.class = "text-danger"})
    </div>
</div>

<div class="form-group">
    @Html.LabelFor(Function(model) model.telephone, htmlAttributes:=New With {.class = "control-label col-md-2"})
    <div class="col-md-10">
        @Html.EditorFor(Function(model) model.telephone, New With {.htmlAttributes = New With {.class = "form-control"}})
        @Html.ValidationMessageFor(Function(model) model.telephone, "", New With {.class = "text-danger"})
    </div>
</div>

<div class="form-group">
    @Html.LabelFor(Function(model) model.subcribeNewsletter, htmlAttributes:=New With {.class = "control-label col-md-2"})
    <div class="col-md-10">
        <div class="checkbox">
            @Html.EditorFor(Function(model) model.subcribeNewsletter)
            @Html.ValidationMessageFor(Function(model) model.subcribeNewsletter, "", New With {.class = "text-danger"})
        </div>
    </div>
</div>

    <div class="form-group">
        <div class="col-md-offset-2 col-md-10">
            <input type="submit" class="btn btn-default" value="Register" />
        </div>
    </div>
    </text>
End Using

@section Scripts
    @Scripts.Render("~/bundles/jqueryval")
End Section
