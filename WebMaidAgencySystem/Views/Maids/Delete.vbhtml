﻿@ModelType WebMaidAgencySystem.Maid
@Code
    ViewData("Title") = "Delete"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

<h2>Delete</h2>

<h3>Are you sure you want to delete this?</h3>
<div>
    <h4>Maid</h4>
    <hr />
    <dl class="dl-horizontal">
        <dt>
            @Html.DisplayNameFor(Function(model) model.User.user_name)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.User.user_name)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.maid_name)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.maid_name)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.birth_date)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.birth_date)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.country)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.country)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.cost_monthly)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.cost_monthly)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.employment_date)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.employment_date)
        </dd>

    </dl>
    @Using (Html.BeginForm())
        @Html.AntiForgeryToken()

        @<div class="form-actions no-color">
            <input type="submit" value="Delete" class="btn btn-default" /> |
            @Html.ActionLink("Back to List", "Index")
        </div>
    End Using
</div>
