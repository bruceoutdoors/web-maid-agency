﻿@ModelType WebMaidAgencySystem.Maid
@Code
    ViewData("Title") = "Details"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

<h2>Details</h2>

<div>
    <h4>Maid</h4>
    <hr />
    <dl class="dl-horizontal">
        <dt>
            @Html.DisplayNameFor(Function(model) model.User.user_name)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.User.user_name)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.maid_name)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.maid_name)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.birth_date)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.birth_date)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.country)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.country)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.cost_monthly)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.cost_monthly)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.employment_date)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.employment_date)
        </dd>

    </dl>
</div>
<p>
    @If Account.IsAdmin Then
        @Html.ActionLink("Edit", "Edit", New With { .id = Model.Id }) 
    Else
        @Html.ActionLink("Employ", "Employ",
                         New With {.id = Model.Id},
                         New With {.class = "btn btn-primary"})
    End If
    @(" | ")
    @Html.ActionLink("Back to List", "Index")
</p>
