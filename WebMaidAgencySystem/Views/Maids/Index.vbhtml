﻿@ModelType IEnumerable(Of WebMaidAgencySystem.Maid)
@Code
ViewData("Title") = "Index"
Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

<h2>Index</h2>
@If Account.IsAdmin Then
    @<p>
    @Html.ActionLink("Create New", "Create")
    </p>
End If

<form method="get">
    <div class="form-group col-md-2">
        <label for="minAmount">Min Amount</label>
        <input type="number" name="min_amount" id="minAmount" class="form-control"/>
    </div>
    <div class="form-group col-md-2">
        <label for="maxAmount">Max Amount</label>
        <input type="number" name="max_amount" id="maxAmount" class="form-control" />
    </div>
    <div class="form-group col-md-3">
        <label for="maidName">Maid Name</label>
        <input type="text" name="name" id="maidName" class="form-control" />
    </div>
    <div class="form-group col-md-3">
        <label for="country">Country</label>
        <input type="text" name="country" id="country" class="form-control" />
    </div>
        <input type="submit" class="btn btn-default"/>


</form>
<table class="table">
    <tr>
        @If Account.IsAdmin Then
        @<th>
            @Html.DisplayNameFor(Function(model) model.User.user_name)
        </th>
        End If
        <th>
            @Html.DisplayNameFor(Function(model) model.maid_name)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.birth_date)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.country)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.cost_monthly)
        </th>
        @If Account.IsAdmin Then
        @<th>
            @Html.DisplayNameFor(Function(model) model.employment_date)
        </th>
        End If    
        <th></th>
    </tr>
@Code
    Dim maidList As IEnumerable(Of WebMaidAgencySystem.Maid)
    maidList = Model.Where(Function(q) q.UserID Is Nothing)
    If Not Request.QueryString("max_amount") = vbNullString Then
        Dim max_amount As Decimal = Request.QueryString("max_amount")
        maidList = maidList.Where(Function(q) q.cost_monthly < max_amount)
    End If
    If Not Request.QueryString("min_amount") = vbNullString Then
        Dim min_amount As Decimal = Request.QueryString("min_amount")
        maidList = maidList.Where(Function(q) q.cost_monthly > min_amount)
    End If
    If Not Request.QueryString("name") = vbNullString Then
        Dim maid_name As String = Request.QueryString("name")
        maidList = maidList.Where(Function(q) q.maid_name.ToUpper Like "*" + maid_name.ToUpper + "*")
    End If
    If Not Request.QueryString("country") = vbNullString Then
        Dim country As String = Request.QueryString("country")
        maidList = maidList.Where(Function(q) q.country.ToUpper Like "*" + country.ToUpper + "*")
    End If
End Code
@For Each item In maidList
    @<tr>
        @If Account.IsAdmin Then
        @<td>
            @Html.DisplayFor(Function(modelItem) item.User.user_name)
        </td>
        End If
        <td>
            @Html.DisplayFor(Function(modelItem) item.maid_name)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.birth_date)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.country)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.cost_monthly)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.employment_date)
        </td>
        <td>
            @Html.ActionLink("Details", "Details", New With {.id = item.Id })
            @If Account.IsAdmin Then
                @("|")
                @Html.ActionLink("Edit", "Edit", New With {.id = item.Id }) @(" | ")
                @Html.ActionLink("Delete", "Delete", New With {.id = item.Id })
            End If
        </td>
    </tr>
Next

</table>
