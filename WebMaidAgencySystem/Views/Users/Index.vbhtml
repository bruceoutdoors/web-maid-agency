﻿@ModelType IEnumerable(Of WebMaidAgencySystem.User)
@Code
ViewData("Title") = "Index"
Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

<h2>Index</h2>

<p>
    @Html.ActionLink("Create New", "Create")
</p>
<table class="table">
    <tr>
        <th>
            @Html.DisplayNameFor(Function(model) model.UserType.type)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.user_name)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.password)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.email)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.subcribeNewsletter)
        </th>
        <th></th>
    </tr>

@For Each item In Model
    @<tr>
        <td>
            @Html.DisplayFor(Function(modelItem) item.UserType.type)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.user_name)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.password)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.email)
        </td>
  
        <td>
            @Html.DisplayFor(Function(modelItem) item.subcribeNewsletter)
        </td>
        <td>
            @Html.ActionLink("Edit", "Edit", New With {.id = item.Id }) |
            @Html.ActionLink("Details", "Details", New With {.id = item.Id }) |
            @Html.ActionLink("Delete", "Delete", New With {.id = item.Id })
        </td>
    </tr>
Next

</table>
