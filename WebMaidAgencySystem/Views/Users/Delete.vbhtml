﻿@ModelType WebMaidAgencySystem.User
@Code
    ViewData("Title") = "Delete"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

<h2>Delete</h2>

<h3>Are you sure you want to delete this?</h3>
<div>
    <h4>User</h4>
    <hr />
    <dl class="dl-horizontal">
        <dt>
            @Html.DisplayNameFor(Function(model) model.UserType.type)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.UserType.type)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.user_name)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.user_name)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.password)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.password)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.email)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.email)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.address1)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.address1)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.address2)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.address2)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.city)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.city)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.postcode)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.postcode)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.telephone)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.telephone)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.subcribeNewsletter)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.subcribeNewsletter)
        </dd>

    </dl>
    @Using (Html.BeginForm())
        @Html.AntiForgeryToken()

        @<div class="form-actions no-color">
            <input type="submit" value="Delete" class="btn btn-default" /> |
            @Html.ActionLink("Back to List", "Index")
        </div>
    End Using
</div>
